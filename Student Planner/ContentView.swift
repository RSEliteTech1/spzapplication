//
//  ContentView.swift
//  Student Planner
//
//  Created by Rohan Shah and Ishan Tandon on 4/11/20.
//  Copyright © 2020 RohanShah. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
